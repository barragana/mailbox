// server.js
const express = require('express');
const path = require('path');
const compression = require('compression');

const app = express();

// must be first!
app.use(compression());
// serve our static stuff like index.css
app.use('/static', express.static(path.resolve(__dirname, '../dist')));

// send all requests to index.html so browserHistory in React Router works
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../dist', 'index.html'));
});

app.get('/static/messages', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../dist', 'messages'));
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log('Production Express server running at localhost: %s', PORT);
});
