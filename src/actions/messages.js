import {
  LOAD_MESSAGES,
  OPEN_READ_MESSAGE,
  DELETE_MESSAGE,
} from '../constants/messages';

export function loadMessages () {
  return {
    type: LOAD_MESSAGES,
  };
}

export function openReadMessage (uid) {
  return {
    type: OPEN_READ_MESSAGE,
    uid,
  };
}

export function deleteMessage (uid) {
  return {
    type: DELETE_MESSAGE,
    uid,
  };
}
