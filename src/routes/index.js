import React from 'react';
import { Route } from 'react-router';
import App from '../containers/App';
import Mailbox from '../containers/Mailbox';
import MailReader from '../containers/MailReader';

export default (
  <Route component={App} >
    <Route path="/" component={Mailbox}>
      <Route path="/:uid" component={MailReader} />
    </Route>
  </Route>
);
