import React, { PropTypes } from 'react';
import moment from 'moment';

const MailItem = ({ uid, sender, subject, time_sent, read, onClick, onDelete }) => (
  <div className={`mail-item ${read ? '' : 'highlight'}`} onClick={() => onClick(uid)}>
    <span>{sender}</span>
    <span>{subject}</span>
    <span>{moment(time_sent).format('ddd DD MMMM, hh:mm')}</span>
    <div className="delete" onClick={(e) => { e.stopPropagation(); onDelete(uid); }}>
      <i className="fa fa-trash-o" />
    </div>
  </div>
);

MailItem.propTypes = {
  uid: PropTypes.string.isRequired,
  sender: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  time_sent: PropTypes.number.isRequired,
  read: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default MailItem;
