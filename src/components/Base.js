import React, { PropTypes } from 'react';
import { Bootstrap } from './bootstrap';

import './base.scss';

const Base = ({ children }) => (
  <Bootstrap>
    <div className="container">
      {children}
    </div>
  </Bootstrap>
);

Base.propTypes = {
  children: PropTypes.node,
};

export default Base;
