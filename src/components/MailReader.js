import React, { PropTypes } from 'react';
import moment from 'moment';

const MailReader = ({ sender, subject, message, time_sent, onClose }) => (
  <div className="mail-reader">
    <div className="close-read-mail">
      <i className="pull-left fa fa-reply" onClick={onClose} />
    </div>
    <div className="mail">
      <span className="subject">{subject}</span>
      <div className="header">
        <span className="sender">{sender}</span>
        <span className="time">{moment(time_sent).format('ddd DD MMMM, hh:mm')}</span>
      </div>
      <div>{message}</div>
    </div>
  </div>
);

MailReader.propTypes = {
  sender: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  time_sent: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default MailReader;
