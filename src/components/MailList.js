import React, { PropTypes } from 'react';

import MailItem from './MailItem';

const MailList = ({ messages, actions }) => (
  <div className="mail-list">
    {messages.map((message) =>
      <MailItem key={`mailitem-${message.uid}`} {...message} {...actions} />
    )}
  </div>
);

MailList.propTypes = {
  messages: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
};

export default MailList;
