import React, { PropTypes } from 'react';

// import 'bootstrap-sass/assets/javascripts/bootstrap.js';
import './bootstrap.scss';


const Bootstrap = ({ children }) => (
  <div>{children}</div>
);


Bootstrap.propTypes = {
  children: PropTypes.any.isRequired,
};


export default Bootstrap;
