import React, { PropTypes } from 'react';

import Base from '../components/Base';

import './app.scss';
// If use stateless function react hot realod does not work
class App extends React.Component {
  render () {
    return (
      <Base>
        { this.props.children }
      </Base>
    );
  }
}

App.propTypes = {
  children: PropTypes.node,
};

export default App;
