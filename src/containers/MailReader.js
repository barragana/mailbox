import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import MailReaderComponent from '../components/MailReader';

class MailReader extends React.Component {

  constructor (props) {
    super(props);
    this.state = this.getEmail(props);
    this.onClose = this.onClose.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    this.setState(this.getEmail(nextProps));
  }

  onClose () {
    this.context.router.push('/');
  }

  getEmail (props) {
    const { params: { uid }, messages } = props;
    return messages.find((message) => message.uid === uid);
  }

  render () {
    return <MailReaderComponent {...this.state} onClose={this.onClose} />;
  }
}

MailReader.contextTypes = {
  router: React.PropTypes.object,
};

MailReader.propTypes = {
  params: PropTypes.shape({
    uid: PropTypes.string.isRequired,
  }).isRequired,
  messages: PropTypes.array.isRequired,
};

function mapStateToProps (state) {
  return {
    messages: state.messages,
  };
}

const MailReaderContainer = connect(mapStateToProps)(MailReader);

export default MailReaderContainer;
