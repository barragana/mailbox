import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import actions from '../actions';

import MailList from '../components/MailList';

class Mailbox extends React.Component {

  constructor (props) {
    super(props);
    this.onOpenEmail = this.onOpenEmail.bind(this);
  }

  onOpenEmail (uid) {
    this.props.openReadMessage(uid);
    this.context.router.push(`/${uid}`);
  }

  renderMailList () {
    const { messages, deleteMessage } = this.props;
    const itemActions = {
      onClick: this.onOpenEmail,
      onDelete: deleteMessage,
    };

    return (
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <MailList messages={messages} actions={itemActions} />
        </div>
      </div>
    );
  }

  renderMailListWhenReading () {
    const { messages, children, deleteMessage } = this.props;
    const itemActions = {
      onClick: this.onOpenEmail,
      onDelete: deleteMessage,
    };

    return (
      <div className="row">
        <div className="hidden-xs hidden-sm col-md-6 col-lg-6">
          <MailList messages={messages} actions={itemActions} />
        </div>
        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          {children}
        </div>
      </div>
    );
  }

  render () {
    return this.props.params.uid ?
      this.renderMailListWhenReading() :
      this.renderMailList();
  }
}

Mailbox.contextTypes = {
  router: React.PropTypes.object,
};


Mailbox.propTypes = {
  messages: PropTypes.array.isRequired,
  params: PropTypes.shape({
    uid: PropTypes.string,
  }),
  children: PropTypes.node,
  deleteMessage: PropTypes.func.isRequired,
  openReadMessage: PropTypes.func.isRequired,
};

function mapStateToProps (state) {
  return {
    messages: state.messages,
  };
}

const MailboxContainer = connect(mapStateToProps, actions)(Mailbox);

export default MailboxContainer;
