/*  eslint-disable global-require */
import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
// import thunk from 'redux-thunk';
// import api from '../middleware/api';
import reducer from '../reducers';

const store = createStore(
  reducer,
  applyMiddleware(createLogger())
);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('../reducers', () => {
    const nextRootReducer = require('../reducers');

    store.replaceReducer(nextRootReducer);
  });
}

export default store;
