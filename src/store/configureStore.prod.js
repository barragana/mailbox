/*  eslint-disable global-require */
import { createStore, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// import api from '../middleware/api';
import reducer from '../reducers';

const store = createStore(
  reducer
  // applyMiddleware(thunk, api)
);

export default store;
