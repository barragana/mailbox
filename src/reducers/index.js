import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { default as messages } from './messages';

const reducer = combineReducers({
  messages,
  routing,
});

export default reducer;
