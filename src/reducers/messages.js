import {
  LOAD_MESSAGES,
  DELETE_MESSAGE,
  OPEN_READ_MESSAGE,
} from '../constants/messages';

const data = process.env.messages;

export default function (state = [], action) {
  switch (action.type) {
    case OPEN_READ_MESSAGE: {
      const index = state.findIndex((message) => message.uid === action.uid);
      const nextState = [].concat(state);
      nextState[index].read = true;
      return nextState;
    }
    case LOAD_MESSAGES:
      return data.messages;
    case DELETE_MESSAGE:
      return state.filter((message) => message.uid !== action.uid);
    default:
      return state;
  }
}
