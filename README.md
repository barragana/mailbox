# README #

### What is this repository for?

* It is a prototype of a mailbox built with React + Redux

### How do I get set up? ###

* npm install

### How do I run it? ###

1 - production

* npm run build
* npm start

2 - development

* npm run dev

### Consideration ###

* Assuming that for small screen of mobiles put navigation sidebar and email reading
side by side would become too small both, when open in mobile the email overlays
the list of emails

* It would be nice to have some test for the reducer to guarantee its states changes
* It would be nice to have some tests for the list, item  and mailreader components
