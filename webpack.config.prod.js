/* eslint-disable */
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: './src/index',
  output: {
    path: __dirname + '/dist',
    publicPath: '/static/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      '__DEVTOOLS__': false,
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.messages': JSON.stringify(require('./data/messages')),
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: { warnings: false },
    }),
    new ExtractTextPlugin('bundle.css', { allChunks: true })
  ],
  module: {
    loaders: [{
      test: /(\.js)?$/,
      exclude: /node_modules/,
      loader: 'react-hot!babel'
    },
    {
      test: require.resolve('bootstrap-sass/assets/javascripts/bootstrap'),
      loaders: [ 'imports?$=jquery,jQuery=jquery' ],
    },
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('style', 'css!sass'),
    },
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style', 'css'),
    },
    {
      test: /(\.woff2?|\.ttf|\.eot|\.svg)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file',
    }],
  },
  resolve: {
    root: __dirname + '/src',
    extensions: ['', '.js']
  }
};
