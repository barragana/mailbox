import React, { PropTypes, Component } from 'react';
// This is used to wrap stateless components in order to be tested
class Wrapper extends Component {
  render () {
    return (
      <div className="trip">
        {this.props.children}
      </div>
    );
  }
}

Wrapper.propTypes = {
  children: PropTypes.node,
};

export default Wrapper;
