/* eslint-disable */
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: __dirname + '/dist',
    publicPath: '/static/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './dist',
    hot: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      __DEVTOOLS__: true,
      'process.env.NODE_ENV': JSON.stringify('development'),
      'process.env.messages': JSON.stringify(require('./data/messages')),
    }),
    new ExtractTextPlugin('bundle.css', { allChunks: true })
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'react-hot!babel'
    },
    {
      test: require.resolve('bootstrap-sass/assets/javascripts/bootstrap'),
      loaders: [ 'imports?$=jquery,jQuery=jquery' ],
    },
    {
      test: /\.scss$/,
      loaders: ["style", "css", "sass"],
    },
    {
      test: /\.json$/,
      loader: 'json',
    },
    {
      test: /(\.woff2?|\.ttf|\.eot|\.svg)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file',
    }]
  },
  resolve: {
    root: __dirname + '/src',
    extensions: ['', '.js']
  }
};
